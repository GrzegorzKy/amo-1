function o=sfer2kart(m)
    %x
    o(1)=m(3)*cosd(m(2))*cosd(m(1));
    %y
    o(2)=m(3)*sind(m(1))*cosd(m(2));
    %z
    o(3)=m(3)*sind(m(2));
end