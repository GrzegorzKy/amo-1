clc
clear
format longG
poziom_morza=6378137;
c=physconst('LightSpeed');
%wsp sferyczne (dlugosc,szeroko��, wysoko�� npm)
% N +
% S-
% E+
% W-
%Po�o�enie satelit�w -dl,szmwys / w km
P1s=[52.885907 13.395837 2e7+poziom_morza];
P2s=[50.312052 12.373351 2e7+poziom_morza];
P3s=[47.796902 19.381854 2e7+poziom_morza];
P4s=[50.619584 26.244260 2e7+poziom_morza];
P5s=[55.488272 28.787526 2e7+poziom_morza];
%po�o�enie satelit�w w kartezja�skim
p1k=sfer2kart(P1s);
p2k=sfer2kart(P2s);
p3k=sfer2kart(P3s);
p4k=sfer2kart(P4s);
p5k=sfer2kart(P5s);
%czasy dotarcia sygna�u
t=[6.685154224579776e-2,
6.688721012125939e-02,
6.678326376379981e-02
,6.673988162655070e-02
,6.684597646875382e-02];
%droga sygna��w
r=c*t;
%r�wnanie
%sum(X-X0(1)^2+(Y-Y0(2)^2+(Z-Z0(3))=r^2
X0=[p1k(1), p2k(1), p2k(1), p3k(1), p4k(1), p5k(1)];
Y0=[p1k(2) p2k(2) p2k(2) p3k(2) p4k(2) p5k(2)];
Z0=[p1k(3) p2k(3) p2k(3) p3k(3) p4k(3) p5k(3)];

% x0 = [0 0 0] ;                       % Starting guess
% f = @(x)myfun(x,X0,Y0,Z0,r);    %ustawiamy handler
% % options.Algorithm = 'levenberg-marquardt';
% % options.Jacobian = 'on';
% options = optimoptions('lsqnonlin','Jacobian','on','Algorithm','levenberg-marquardt','TolFun',1e-8);
% [x,resnorm] = lsqnonlin(f,x0,[],[],options);   % Invoke optimizer
%test
x0 = sfer2kart([210000000.020 -1000002.259 -poziom_morza])                       % Starting guess
options = optimoptions('lsqnonlin','Jacobian','off','Algorithm','levenberg-marquardt','TolX',1e-5,'TolFun',1e-3);
f = @(x)test(x,X0,Y0,Z0,r);
x = lsqnonlin(f,x0,[],[],options);   % Invoke optimizer
% x=kart2sfer(x)
%kart2sfer(x)
norm(x)-poziom_morza
