function [F,J] = myfun(x,X0,Y0,Z0,R)
F=zeros(1);
J=zeros(1,3);
for i=1:5
F = F + ((x(1)-X0(i)).^2+(x(2)-Y0(i)).^2+(x(3)-Z0(i))-R(i).^2);
J(1) = J(1)+ (2*x(1)-2*X0(i));
J(2) = J(2)+ (2*x(2)-2*Y0(i));
J(3) = J(3)+ (2*x(3)-2*Z0(i));
end
