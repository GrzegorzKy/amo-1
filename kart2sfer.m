function o=kart2sfer(m)
    %dlugosc
    o(1)=atand(m(2)/m(1));   
    %promien
    o(3)=norm(m);
     %szserokosc
    o(2)=asind(m(3)/o(3));
end